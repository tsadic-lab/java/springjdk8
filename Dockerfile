FROM openjdk:8u92-jdk-alpine

COPY . /app
WORKDIR /app
RUN ./mvnw clean install
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "target/app.jar"]
